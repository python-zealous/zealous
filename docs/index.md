# Introduction

Zealous is a framework that aims to simplify specification-driven development.

It is built from the ground up with emphasis on static type checking to ensure that the
specification is respected while developing around it.

The framework's core elements run in an asyncio event loop so concurrency is supported
out of the box.

---

**Source Code:** <https://gitlab.com/python-zealous/zealous>

---

## Installation

```bash
pip install zealous
```

Requires Python 3.7 and up.

## Example

### Define the boundary

#### boundary.py

```python
from .resources import user

class Boundary(zealous.Boundary):
    users = user.User()
```

#### resources/user.py

```python
import uuid

from datetime import datetime

from typing import Optional
from typing import Tuple

import zealous

class LookupSchema(zealous.Schema):
    user_id: uuid.UUID

class RegistrationSchema(zealous.Schema):
    username: str
    password: str
    full_name: Optional[str] = None

class Schema(zealous.Schema):
    user_id: uuid.UUID
    name: str
    registered_on: datetime

class User(zealous.Resource):
    # simple CRUD interfaces
    register: zealous.Interface[RegistrationSchema, Schema]
    get_details: zealous.Interface[LookupSchema, Schema]
    delete: zealous.Interface[LookupSchema, None]
    list: zealous.InterfaceNoBody[Tuple[Schema, ...]]
```

### Integrate with the boundary using AppMixin

Now that the boundary is well defined, we can integrate with it in confidence from
either the client side or the server side. The functionality around boundaries is
enhanced by Zealous' AppMixins. The RPCClient and RPCServer AppMixins are showcased
below:

#### app/main.py

```python
import asyncio

from zealous import rpc
from zealous.rpc.asyncioq import AsyncioQRPCDriver

from ..boundary import Boundary
from ..resources import user
from ..rpc import server

rpc_driver = AsyncioQRPCDriver(maxsize=1)

class App(
    rpc.RPCClientAppMixin[Boundary],
    rpc.RPCServerAppMixin[Boundary],
):
    __rpc_client__ = rpc.RPCClientConf(driver=rpc_driver)
    __rpc_server__ = rpc.RPCServerConf(
        driver=rpc_driver, impl=server.BoundaryImpl()
    )

async def main() -> None:
    app = rpc.RPCClientApp(Boundary())
    app.rpc_server.start()

    # using mypy as a static analyzer will catch any improper use
    # of the boundary

    client = app.rpc_client

    new_user = client.user.register(
        user.RegistrationSchema(
            username="admin",
            password="password",
        )
    )
    print(f"created user with id {new_user.user_id}")

    client.user.delete(
        user.LookupSchema(user_id=new_user.user_id)
    )
    print(f"user with id {new_user.user_id} was deleted")

if __name__ == "__main__":
    asyncio.run_until_complete(main())
```

#### rpc/server.py
```python
from datetime import datetime
import hashlib
import uuid

from typing import Dict
from typing import Iterable
from typing import Tuple

import zealous

from ..boundary import Boundary
from ..resources import user

class UserDBSchema(zealous.Schema):
    user_id: uuid.UUID
    username: str
    registered_on: datetime
    password_hash: str

USER_DATABASE: Dict[uuid.UUID, UserDBSchema] = {}

class UserImpl(user.User):
    """Crude implementation of User resource."""

    async def register(self, body: user.RegistrationSchema) -> user.Schema:
        new_user = user.Schema(
            user_id=uuid.uuid4(),
            username=body.username,
            registered_on=datetime.now(),
        )

        def hash_password(password):
            return hashlib.sha512(password.encode()).hexdigest()

        USER_DATABASE[new_user.user_id] = UserDBSchema(
            user_id=new_user.user_id,
            username=new_user.username,
            registered_on=new_user.registered_on,
            password_hash=hash_password(body.password),
        )
        return new_user

    @staticmethod
    def __db_schema_to_schema(db_user: UserDBSchema) -> user.Schema:
        return user.Schema(
            user_id=db_user.user_id,
            username=db_user.username,
            registered_on=db_user.registered_on,
        )

    async def list(self) -> Tuple[user.Schema, ...]:
        def generator() -> Iterable[user.Schema]:
            for db_user in USER_DATABASE.values():
                yield self.__db_schema_to_schema(db_user)

        return tuple(generator())

    async def get_details(self, body: user.LookupSchema) -> user.Schema:
        return self.__db_schema_to_schema(USER_DATABASE[body.user_id])

    async def delete(self, body: user.LookupSchema) -> None:
        del USER_DATABASE[body.user_id]

class BoundaryImpl(boundary.Boundary):
    users = UserImpl()
```

## Project dependencies

The following projects make Zealous possible:

### [Pydantic](https://pydantic-docs.helpmanual.io/)

This is the project responsible for the functionality of the zealous.Schema, which is
named "BaseModel" in the Pydantic project. Please visit
[the Pydantic docs](https://pydantic-docs.helpmanual.io/) to learn more
about pydantic.BaseModel.
