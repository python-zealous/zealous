import hashlib
import uuid

import pytest

from tests.rpc import main
from tests.rpc.asyncioq.main import App
from tests.rpc.main import USER_DATABASE
from tests.rpc.main.resources import user
from zealous.rpc import exceptions


pytestmark = pytest.mark.asyncio


async def test_user_management_scenario(app: App) -> None:
    client = app.rpc_client
    assert not USER_DATABASE

    registration_body = user.RegistrationSchema(username="admin", password="password")
    new_user = await client.users.register(registration_body)
    assert new_user.username == "admin"
    assert USER_DATABASE[new_user.user_id] == main.UserDBSchema(
        user_id=new_user.user_id,
        username="admin",
        registered_on=main.NOW,
        password_hash=hashlib.sha512("password".encode()).hexdigest(),
    )

    get_details_body = user.LookupSchema(user_id=new_user.user_id)
    user_details = await client.users.get_details(get_details_body)
    assert user_details == new_user

    user_list = await client.users.list()
    assert user_list == (user_details,)

    delete_body = user.LookupSchema(user_id=new_user.user_id)
    assert await client.users.delete(delete_body) is None
    assert not USER_DATABASE


async def test_unhandled_exception_in_backend(app: App) -> None:
    client = app.rpc_client
    assert not USER_DATABASE

    get_details_body = user.LookupSchema(user_id=uuid.uuid4())
    with pytest.raises(exceptions.RPCRemoteError) as excinfo:
        await client.users.get_details(get_details_body)

    assert excinfo.value.exc_type == "KeyError"
    assert "UUID" in excinfo.value.exc_message


async def test_no_resource_implementation(app: App) -> None:
    client = app.rpc_client

    with pytest.raises(exceptions.RPCRemoteError) as excinfo:
        await client.not_implemented.dummy()

    assert excinfo.value.exc_type == "RPCServerInterfaceNotImplemented"
    assert "dummy" in excinfo.value.exc_message
    assert "not_implemented" in excinfo.value.exc_message


async def test_no_interface_implementation(app: App) -> None:
    client = app.rpc_client

    with pytest.raises(exceptions.RPCRemoteError) as excinfo:
        await client.users.not_implemented()

    assert excinfo.value.exc_type == "RPCServerInterfaceNotImplemented"
    assert "users" in excinfo.value.exc_message
    assert "not_implemented" in excinfo.value.exc_message
