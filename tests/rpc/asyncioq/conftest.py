import asyncio

from typing import AsyncGenerator

import pytest

from tests.rpc.asyncioq.main import App
from tests.rpc.asyncioq.main import rpc_driver
from tests.rpc.main import USER_DATABASE
from tests.rpc.main import Boundary


@pytest.fixture(name="app")
async def fixture_app(
    _event_loop: asyncio.AbstractEventLoop,
) -> AsyncGenerator[App, None]:
    app = App(Boundary())
    await app.rpc_server.start()
    yield app
    await app.rpc_server.stop()
    rpc_driver.reset()
    USER_DATABASE.clear()
