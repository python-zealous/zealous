import zealous

from .resources import user


__all__ = ("Boundary",)


class ResourceNotImplemented(zealous.Resource):
    # other interfaces to test behaviour of framework
    dummy: zealous.InterfaceNoBody[None]


class Boundary(zealous.Boundary):
    users = user.User()
    not_implemented = ResourceNotImplemented()
