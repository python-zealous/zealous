import hashlib
import uuid

from datetime import datetime
from typing import Dict
from typing import Iterable
from typing import Tuple

import zealous

from . import boundary
from .resources import user


__all__ = (
    "BoundaryImpl",
    "NOW",
    "UserDBSchema",
    "USER_DATABASE",
)


# Subclassing Schema's breaks the static type checker because the return type is
# covariant. I really want it to be invariant, but this doesn't work right now:
#   https://github.com/python/mypy/issues/5775
# class UserDBSchema(user.Schema):
#     password_hash: str


class UserDBSchema(zealous.Schema):
    user_id: uuid.UUID
    username: str
    registered_on: datetime
    password_hash: str


NOW = datetime.now()
USER_DATABASE: Dict[uuid.UUID, UserDBSchema] = {}


class UserImpl(user.User):
    """Crude implementation of User resource."""

    async def register(self, body: user.RegistrationSchema) -> user.Schema:
        new_user = user.Schema(
            user_id=uuid.uuid4(), username=body.username, registered_on=NOW
        )
        USER_DATABASE[new_user.user_id] = UserDBSchema(
            user_id=new_user.user_id,
            username=new_user.username,
            registered_on=new_user.registered_on,
            password_hash=hashlib.sha512(body.password.encode()).hexdigest(),
        )
        return new_user

    @staticmethod
    def __db_schema_to_schema(db_user: UserDBSchema) -> user.Schema:
        return user.Schema(
            user_id=db_user.user_id,
            username=db_user.username,
            registered_on=db_user.registered_on,
        )

    async def list(self) -> Tuple[user.Schema, ...]:
        def generator() -> Iterable[user.Schema]:
            for db_user in USER_DATABASE.values():
                yield self.__db_schema_to_schema(db_user)

        return tuple(generator())

    async def get_details(self, body: user.LookupSchema) -> user.Schema:
        return self.__db_schema_to_schema(USER_DATABASE[body.user_id])

    async def delete(self, body: user.LookupSchema) -> None:
        del USER_DATABASE[body.user_id]


class BoundaryImpl(boundary.Boundary):
    users = UserImpl()
